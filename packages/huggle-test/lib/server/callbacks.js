import { addCallback } from "meteor/vulcan:lib";

async function userVerification(validationErrors, properties) {
  const { document } = properties;
  const { birthday } = document;
  if (birthday > 120) {
    validationErrors.push("Votre age doit etre compris entre 0 et 120 ans");
  }
  return validationErrors;
}

addCallback("Candidates.create.validate", userVerification);
addCallback("Candidates.update.validate", userVerification);
