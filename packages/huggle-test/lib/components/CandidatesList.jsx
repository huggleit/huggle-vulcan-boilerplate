import React from "react";
import Typography from "@material-ui/core/Typography";
import { Components } from "meteor/vulcan:core";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import moment from "moment";

const columns = [
  {
    name: "firstName",
    sortable: true,
    filterable: true,
  },
  {
    name: "lastName",
    sortable: true,
    filterable: true,
  },
  {
    name: "isValidated",
    sortable: true,
    filterable: true,
    component: ({ document }) => <Checkbox
      checked={document.isValidated}
    />,
  },
  {
    name: "createdAt",
    sortable: true,
    component: ({ document }) =>
      <Typography>{moment(document.createdAt).format("DD/MM/YYYY")}</Typography>,
  },  {
    name: "updatedAt",
    sortable: true,
    component: ({ document }) =>
      <Typography>{moment(document.updatedAt).format("DD/MM/YYYY")}</Typography>,
  },
];
export const CandidatesList = () => {
  return (
    <React.Fragment>
      <Grid container justify={"center"} alignItems={"center"}>
        <Typography color={"primary"}>La liste des Candidats </Typography>
      </Grid>
      <Components.Datatable
        collectionName={"Candidates"}
        fragmentName={"CandidatesFragment"}
        columns={columns}
        showDelete={true}
        showSearch={true}
        emptyState={<Typography>Pas de Candidats </Typography>}
        newFormProps={{
          label:"Ajouter un candidat",
          submitLabel:"Ajouter"
        }}
        editFormProps={{
          label:"Modifier un candidat",
          submitLabel:"Modifier"
        }}
      />
    </React.Fragment>
  );
};

export default CandidatesList;
