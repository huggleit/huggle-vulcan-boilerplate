import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { Components } from "meteor/vulcan:core";


export const CandidatesForm = () => {

  return <Grid
    container
    justify={"center"}
    alignItems={"center"}
    direction={"column"}
  >
    <Typography color={"primary"}>
      Bienvenu sur le formulaire de candidature d'Huggle
    </Typography>
    <Components.SmartForm
      collectionName={"Candidates"}
      fields={["firstName", "lastName", "birthday"]}
      submitLabel="Valider"
    />
  </Grid>;
};
export default CandidatesForm;
