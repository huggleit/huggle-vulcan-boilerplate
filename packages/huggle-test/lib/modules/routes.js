import { addRoute } from "meteor/vulcan:lib";
import { CandidatesForm, CandidatesList } from "../components";

addRoute({
  name: "CandidatesForms",
  path: "/",
  component: CandidatesForm,
});
addRoute({
  name: "CandidatesList",
  path: "/list",
  component: CandidatesList,
});
