import { registerFragment } from "meteor/vulcan:core";

registerFragment(`
  fragment CandidatesFragment on Candidates {
    _id
    firstName
    lastName
    birthday
    isValidated
  }
`);
