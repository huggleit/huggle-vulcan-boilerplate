import SimpleSchema from "simpl-schema";


export default new SimpleSchema({
  _id: {
    type: String,
    canRead: ["guests"],
    optional: true,
  },
  lastName: {
    type: String,
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    label: "Nom",
    optional: false,
    searchable: true,
  },
  firstName: {
    type: String,
    label: "Prénom",
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    optional: false,
    searchable: true,
  },
  birthday: {
    type: Number,
    label: "Age",
    optional: false,
    min:0,
    max:120,
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    inputProperties: {
      help: "Age compris entre 0 et 120 ans",
      required:true
    },
  },
  isValidated: {
    type: Boolean,
    optional: true,
    defaultValue: false,
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    input: "checkbox",
    label: "Utilisateur validé",
  },
  createdAt: {
    type: Date,
    optional: true,
    canRead: ["guests"],
    onCreate: () => new Date(),
    label: "Date de Création",
  },
  updatedAt: {
    type: Date,
    optional: true,
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    hidden: true,
    label: "Date de modification",
    onInsert: () => new Date(),
    onUpdate: () => new Date(),
  },
});
