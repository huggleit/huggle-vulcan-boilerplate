import { createCollection } from "meteor/vulcan:core";

import schema from "./schema";

export const HuggleCandidates = createCollection({
  collectionName: "Candidates",
  typeName: "Candidate",
  schema,
  permissions: {
    canRead: ["guests"],
    canCreate: ["guests"],
    canUpdate: ["guests"],
    canDelete: ["guests"],
  },
});

export default HuggleCandidates;
