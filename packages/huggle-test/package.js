Package.describe({
  name: "huggle-test",
  summary: "Huggle Test technique",
});

Package.onUse(function (api) {
  api.use([
    "vulcan:core",
    "vulcan:forms"
  ]);

  api.mainModule("lib/server/main.js", "server");
  api.mainModule("lib/client/main.js", "client");
});

Package.onTest(function (api) {
  api.use([
    "vulcan:core",
  ]);
});
